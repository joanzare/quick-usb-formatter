/*
    <Class representing the USB devices currently connected to the system>
    Copyright (C) <2011>  <Lisa "shainer" Vitolo>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "devices.h"

using namespace Solid;

/*
 * Custom exception, mainly used for debugging and because I need something at the end of search methods
 * If it appears during the execution, something is not right!
 */
struct DeviceNotFoundException : public std::exception
{
  virtual const char* what() const throw()
  {
    return "[BUG] DeviceList: the device wasn't present in list";
  }
};

void DeviceList::initOperation()
{
  /*
   * If isNotifier is true, the program was called from the device notifier, so only the device that caused the
   * notifier to appear is considered at the beginning
   */
  if (isNotifier)
  {
    addDeviceToList(devNotifier);
  }
  else /* Otherwise, check for devices */
  {
    initList();
  }
  
  QDBusConnection dbus(QDBusConnection::systemBus());
  if (!dbus.isConnected())
  {
    qDebug() << "FATAL: error while connecting to DBus system";
  }
  const QString serviceName("org.freedesktop.UDisks");
  const QString servicePath("/org/freedesktop/UDisks");
  const QString signalAdd("DeviceAdded");
  const QString signalRm("DeviceRemoved");
  
  dbus.connect(serviceName, servicePath, serviceName, signalAdd, this, SLOT(addDevice(QDBusObjectPath)));
  dbus.connect(serviceName, servicePath, serviceName, signalRm, this, SLOT(removeDevice(QDBusObjectPath)));
}

void DeviceList::addDevice(QDBusObjectPath dev)
{
  QString fullPath = dev.path();
  
  /*
   * Sometimes when a new device is added two signals are sent: one for /dev/sdX and another one
   * for /dev/sdXN: we need only this second one
   */
  if (!(fullPath[fullPath.size() - 1].isNumber()))
  {
    return;
  }
  
  /*
   * Compares the current device with a list maintained by Solid to find the right Device object.
   */
  foreach (Device device, Device::allDevices())
  {
    if (fullPath.compare(device.udi()) == 0)
    {
      addDeviceToList(device);
    }
    
  }
}

USBDevice DeviceList::findDeviceFromPath(QString path)
{
  
  for (std::vector<USBDevice>::iterator it = devices.begin(); it < devices.end(); it++)
  {
    if (path.compare(it->getPath()) == 0)
    {
      return (*it);
    }
  }
  
  throw DeviceNotFoundException();
}

void DeviceList::setDeviceNotifier(Device const& dev)
{
  devNotifier = dev;
  isNotifier = true;
}


void DeviceList::removeDevice (QDBusObjectPath dev)
{
  QString path = getDevPath(dev.path());
  
  if (!(path[path.size() - 1].isNumber()))
  {
    return;
  }
  
  for (std::vector<USBDevice>::iterator it = devices.begin(); it < devices.end(); it++)
  {
    if (path.compare(it->getPath()) == 0)
    {
      emit refreshDevices((*it), false);
      devices.erase(it);
      return;
    }
  }
  
  throw DeviceNotFoundException();
}

QString DeviceList::getDevPath(QString s)
{
  return "/dev/" + s.section('/', 5, 5); /* removes useless stuff from the path */
}


void DeviceList::initList()
{
  int count = 0;
  
  foreach(const Device &i, Device::listFromType(DeviceInterface::StorageAccess, QString()))
  {
    if(i.parent().is<StorageDrive>() && i.parent().as<StorageDrive>()->bus() == StorageDrive::Usb)
    {
      addDeviceToList(i);
      count++;
    }
  }
  
  /*
   * **HACK ALERT**
   * If no device is present, the DeviceRemoved signal is never sent (of course!) so the GUI doesn't receive any notify
   * and doesn't disable the GUI as it normally do when there's no device.
   * So a dummy device (no parameters are added, empty constructor) is created to trigger the right control in the GUI.
   */
  if (count == 0)
  {
    USBDevice dummy;
    emit refreshDevices(dummy, false);
  }
}

void DeviceList::addDeviceToList(Device d)
{
  QString path = getDevPath(d.udi());  
  USBDevice n(path, d);
  devices.push_back(n);
  emit refreshDevices(n, true);
}

#include "devices.moc"
