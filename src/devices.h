/*
    <Class representing the USB devices currently connected to the system: header file>
    Copyright (C) <2011>  <Lisa "shainer" Vitolo>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef DEVICES_H
#define DEVICES_H

#include <QtCore>
#include <QString>
#include <QObject>

/*
 * Dbus classes for device-related events
 */
#include <QDBusConnection>
#include <QDBusObjectPath>
#include <QtCore>
#include <QtGui>

#include <solid/device.h>
#include <solid/storageaccess.h>
#include <solid/storagevolume.h>	
#include <solid/storagedrive.h>
#include <solid/block.h>

#include <exception>
#include <vector>

#include "usbdevice.h"

using namespace Solid;

class DeviceList : public QObject
{
  Q_OBJECT
  
public:
    DeviceList() : isNotifier(false) {}
    USBDevice findDeviceFromPath(QString);
    void initOperation();
    void setDeviceNotifier(Device const&);
  
  signals:
    void refreshDevices(USBDevice, bool);
    
private:
    std::vector<USBDevice> devices;
    Device devNotifier;
    bool isNotifier;
    
    void initList();
    void addDeviceToList(Device);
    QString getDevPath(QString);
    
private slots:
    void addDevice(QDBusObjectPath dev);
    void removeDevice(QDBusObjectPath dev);
    
};

#endif // DEVICES_H
