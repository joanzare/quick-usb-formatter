/*
    <Execute the format command.>
    Copyright (C) 2012  José Antonio Sánchez Reynaga <jsanchez@estrasol.com.mx>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "helper.h"
#include <klocalizedstring.h>

/**
 
 */
ActionReply Helper::format(QVariantMap args)
{
    
    ActionReply reply = ActionReply::SuccessReply;
    
    
    // Is mounted ??
    if( args["mounted"].toBool() ){
        qDebug() << "Umounting device : " << args["device"].toString() ;
        
        if(QProcess::execute("umount", QStringList() << args["device"].toString() ) != 0){
            reply = ActionReply::HelperErrorReply;
            reply.setErrorDescription( i18n("umount failed") );
            return reply;
        }
    }
    
    QStringList arguments;
    arguments.append("-c");
    QString command =  args["command"].toString() + QString(" ") +
	args["args"].toStringList().join(" ");
    
    arguments.append(command);
    
    qDebug() << "Executing: /bin/bash -c" << command;
    
    QProcess process;
    //process->setEnvironment( QProcess::systemEnvironment() );
    process.setProcessEnvironment(QProcessEnvironment::systemEnvironment());
    process.start("/bin/bash", arguments);
    
    process.waitForFinished(1000 * 60 * 2); // 2 minutes max
    QByteArray errorOutput = process.readAllStandardError();
    QByteArray standarOutput = process.readAllStandardOutput();
    
    
    qDebug() << "ERROR OUTPUT : " << QString::fromLocal8Bit(errorOutput);
    qDebug() << "OUTPUT: "<< QString::fromLocal8Bit(standarOutput);
    qDebug() << "EXIT CODE : " << process.exitCode();
    qDebug() << "ERROR : " << process.errorString();
    qDebug() << "Current Dir : " << process.workingDirectory();
    qDebug() << "Current env : " << process.processEnvironment().toStringList();
    
    if(process.exitCode() != 0){
         reply = ActionReply::HelperErrorReply;
         reply.setErrorDescription(QString::fromLocal8Bit(errorOutput));        
    }
    
  
    return reply;
}    


/**
 * Necesario para que cree la funcion main requerida,
 * para su ejecucion
 */
KDE4_AUTH_HELPER_MAIN("org.kde.auth.quf", Helper);