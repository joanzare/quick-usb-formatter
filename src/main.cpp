

#include <QtGui/QApplication>
#include "window.h"
#include "devices.h"
#include <QtCore>
#include <KDE/KApplication>
#include <KDE/KAboutData>
#include <KDE/KCmdLineArgs>
#include <KDE/KLocale>
using namespace Solid;

static const char description[] =
    I18N_NOOP("Format Devices made easy");

static const char version[] = "0.1";

int main(int argc, char *argv[])
{  
    KAboutData about("quickusbformatter", 0, ki18n("USB Format"), version, ki18n(description),
                     KAboutData::License_GPL_V3, ki18n("(C) 2012 José Antonio Sánchez Reynaga"), KLocalizedString(), 0, "joanzare@gmail.com");
    KCmdLineArgs::init(argc, argv, &about);

    KCmdLineOptions options;
    options.add("+udi <argument>", ki18n( "Device" ));
    KCmdLineArgs::addCmdLineOptions(options);
    KApplication app;
    DeviceList devList;
  
    
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    
    
  /*
   * If exactly three arguments are present, hopefully it was called from the device notifier
   */
  if(args->count())
  {
    Device device(args->arg(1));
    devList.setDeviceNotifier(device);
  }
    
  /* Creating main window */
  Window *w = new Window(devList);
  w->show();
  app.exec();
}



