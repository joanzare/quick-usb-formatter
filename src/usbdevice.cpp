/*
    <Simple class representing an USB device with needed information>
    Copyright (C) <2011>  <Lisa "shainer" Vitolo>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "usbdevice.h"

using namespace Solid;

USBDevice::USBDevice() {}

USBDevice::USBDevice(QString p, Device d) : path(p), device(d) {}

QString USBDevice::getPath()
{
  return path;
}

Device USBDevice::getDevice()
{
  return device;
}

QString USBDevice::getVendor()
{
  return device.vendor();
}

int USBDevice::getCapacity()
{
  QString giga = QVariant(device.as<StorageVolume>()->size()).toString();
  float g = giga.toFloat();
  int gig = (int) (g / 1024) / 1024;
  
  return gig;
}

QString USBDevice::getFilesystem()
{
  return device.as<StorageVolume>()->fsType();
}
