/*
    <Simple class representing an USB device with needed information: header file>
    Copyright (C) <2011>  <Lisa "shainer" Vitolo>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef USBDEVICE_H
#define USBDEVICE_H

#include <QtGui>

#include <solid/device.h>
#include <solid/storagevolume.h>

using namespace Solid;

class USBDevice
{

public:
    USBDevice();
    USBDevice(QString, Device);
    
    /* Except from path, this information are extracted from the Solid::Device object */
    QString getPath();
    QString getVendor();
    int getCapacity();
    QString getFilesystem();
    Device getDevice();
    
private:
    QString path;
    Device device;
};

#endif // USBDEVICE_H
