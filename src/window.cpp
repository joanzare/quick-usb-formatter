/*
 *   <Graphical interface>
 *   Copyright (C) <2011>  <José Antonio Sánchez Reynaga>, <Lisa "shainer" Vitolo>
 * 
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 * 
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 * 
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "window.h"
#include "ui_window.h"

#include <QDBusMessage>
#include <QDBusArgument>
#include <klocalizedstring.h>


Window::Window(DeviceList& dl, QWidget *parent) : 
    KXmlGuiWindow(parent),
    ui(new Ui::Window),
    devList(dl)
{
    
    
  QWidget *w = new QWidget(this);
  ui->setupUi(w);
  setCentralWidget(w);
  
  this->busy = false;
  
  /* Device slot */
  connect(&devList, SIGNAL(refreshDevices(USBDevice, bool)), this, SLOT(refreshListDevices(USBDevice, bool)));
  devList.initOperation();
  
  /* GUI slots */
  connect(ui->cb_devices, SIGNAL(activated(QString)), this, SLOT(newDeviceProperties(QString)));
    
  /* Available filesystems will appear here */
  ui->cb_filesystem->addItem("fat32");
  ui->cb_filesystem->addItem("ntfs");
  ui->cb_filesystem->addItem("ext4");
  ui->cb_filesystem->addItem("ext3");
  ui->cb_filesystem->addItem("ext2");
  ui->cb_filesystem->addItem("f2fs");
  ui->cb_filesystem->addItem("exfat");
  
//   connect(ui->pushButton, SIGNAL(clicked(bool), this, SLOT());
  Action *formatAction = new Action("org.kde.auth.quf.format");
  formatAction->setHelperID("org.kde.auth.quf");
  
  KPushButton *pushButton = ui->but_format;
  pushButton->setAuthAction(formatAction);
  
  //Cuando....
  connect(pushButton,SIGNAL(authorized(KAuth::Action*)), SLOT(performAction(KAuth::Action*)));
  
}

Window::~Window() {}

void Window::closeEvent(QCloseEvent* e)
{
    if(this->busy == true){
        int reply = QMessageBox::warning(this, i18n("Are you sure?"), i18n("Really want exit?"), QMessageBox::Yes, QMessageBox::No);
        if(reply == QMessageBox::No)
            e->ignore();
        else
            e->accept();
        
    }   
}

QString Window::escapeLabel(const QString label)
{
  return QString("'") + label + QString("'");
}


/**
 * Llamado cuando se autoriza la ejecucion de la Acción
 */
void Window::performAction(KAuth::Action* action)
{
    
      Device actual = getCurrentDevice();

      if(!actual.as<Block>()->device().contains(QRegExp("([0-9])$"))){
            qWarning() << "WARNING: It is needed to create a partition";
            QMessageBox::warning(this, i18n("Warning"), i18n("It is needed to create a partition"), QMessageBox::Ok);
            return;
      }
     
     // Obtener argumentos para el formato
     QString command = "";
     QStringList args;
     QString filesystem = ui->cb_filesystem->currentText();
     QString label = ui->label_device->text().trimmed();
     QString dirDev = ui->cb_devices->currentText();
     
     if(filesystem == QString("ntfs")){
        qDebug() << "**** FORMATTING AS NTFS";
        
        if(label.isEmpty()) 
            args << "-f" << "-v" << dirDev;
        else 
            args << "-f" << "-L"  << escapeLabel(label)  << "-v" << dirDev;
         
        command = "mkfs.ntfs";
        
    }
    
    else if(filesystem == QString("exfat")){
        qDebug() << "**** FORMATTING AS exFAT";
        
        if(label.isEmpty()) 
            args << dirDev;
        else 
            args  << "-n"  << escapeLabel(label.mid(0, 16)) << dirDev;
         
        command = "mkfs.exfat";
        
    }
    
    else if(filesystem == QString("f2fs")){
        qDebug() << "**** FORMATTING AS Flash-Friendly File System";
        
        if(label.isEmpty()) 
            args << dirDev;
        else 
            args  << "-l"  << escapeLabel(label) << dirDev;
         
        command = "mkfs.f2fs";
        
    }
    

    else if(filesystem == QString("fat32")){
        qDebug() << "**** FORMATTING AS FAT 32";

        if(label.isEmpty())
            args << "-v" << dirDev;
        else
            args << "-n" << escapeLabel((label).toUpper().mid(0, 11)) << "-v" << dirDev;

        command = "mkdosfs";
        
    }

    else if(filesystem == QString("ext4")){
        qDebug() << "**** FORMATTING AS EXT4";

        if(label.isEmpty())
            args << "-t" << "ext4" << "-v" << dirDev;
        else
            args << "-t" << "ext4" << "-L"  << escapeLabel((label).mid(0, 16)) << "-v" << dirDev;

        command = "mke2fs";

    }else if(filesystem == QString("ext2")){

        qDebug() << "**** FORMATTING AS EXT2";

        if(label.isEmpty())
            args << "-t" << "ext2" << "-v" << dirDev;
        else
            args << "-t" << "ext2" << "-L"  << escapeLabel((label).mid(0, 16)) << "-v" << dirDev;
            
        command = "mke2fs";
        
    }else if(filesystem == QString("ext3")){

         qDebug() << "**** FORMATTING AS EXT3";

        if(label.isEmpty())
            args << "-t" << "ext3" << "-v" << dirDev;
        else
            args << "-t" << "ext3" << "-L"  << escapeLabel((label).mid(0, 16)) << "-v" << dirDev;

        command = "mke2fs";
        
    }

    else{
        qDebug() << "ERROR: Unsupported filesystem = " << filesystem;
        this->statusBar()->showMessage( i18n("Failed : Unsupported filesystem %1", filesystem) );
        return;
    }
     
     qDebug() << "EXEC COMMAND : " << command << ", ARGS: " << args;
     
     QVariantMap map;
     map["command"] = command;
     map["args"] = args;
     map["mounted"] = actual.as<StorageAccess>()->isAccessible();
     map["device"] = dirDev;
     action->setArguments(map);
     
     this->statusBar()->showMessage("Wait a moment ...");
     
     this->busy = true;
     this->disableGUI();
     ActionReply reply = action->execute();
     this->busy = false;
     this->enableGUI();
     
     //Syncronous
     if (reply.failed()){
        if (reply.type() == ActionReply::KAuthError) {
            this->statusBar()->showMessage(i18n("Failed: Unable to authenticate/execute the action: %1, %2", reply.errorCode(), reply.errorDescription()));
                // There has been an internal KAuth error
        } else {
                // Our helper triggered a custom error
                // Act accordingly...
           this->statusBar()->showMessage( i18n("Failed : %1", reply.errorDescription()) );
        }
    }
    else {
        this->statusBar()->showMessage("Success");
    }
  
}

/*
 * Called when a new USB device dev is added (added=true) or removed
 * (added=false). Update lists accordingly.
 */
void Window::refreshListDevices(USBDevice dev, bool added)
{
  
  /* No changes while formatting is running */
  if(this->busy)
  {
    return;
  }
  
  if(added == true)
  {
    ui->cb_devices->insertItem(0, dev.getPath());
  }
  else
  {
    int i = ui->cb_devices->findText(dev.getPath());
    
    /*
     * **HACK ALERT**
     * Of course any attempt of removal is made with present devices, so i==-1 (device not present in list)
     * should never be true. However, see DeviceList implementation for an explanation.
     * In addition, when called from device notifier only the "caller" device is added to the list, so this
     * may happen anyway.
     */
    if (i != -1)
    {
      ui->cb_devices->removeItem(i);
    }
    
    /* No more devices! */
    if (ui->cb_devices->count() == 0)
    {
      ui->lb_product->setText(" - ");
      ui->lb_size->setText(" - ");
      ui->lb_filesystem->setText(" - ");
      
      this->statusBar()->showMessage( i18n("No devices found" ) );
    
      disableGUI();
      return;
    }
  }

  ui->cb_devices->setCurrentIndex(0);
  newDeviceProperties(ui->cb_devices->currentText());
  this->statusBar()->showMessage( i18n("Ready" ) );
}

/*
 * Refresh properties of a device when uses selects it on the combobox
 */
void Window::refreshDeviceProperties()
{  
  QString str_device = ui->cb_devices->currentText();
  newDeviceProperties(str_device);
}

void Window::newDeviceProperties(QString devname)
{
  USBDevice usb = devList.findDeviceFromPath(devname);
  QString size;
  size.setNum(usb.getCapacity());
  
  ui->lb_product->setText( usb.getVendor() );
  ui->lb_size->setText( size.append("MB") );
  ui->lb_filesystem->setText( usb.getFilesystem());
  
  // Icon depending of system
  QLabel *label = ui->label_icon;
  if(usb.getDevice().parent().is<StorageDrive>() && usb.getDevice().parent().as<StorageDrive>()->bus() == StorageDrive::Usb ) {

        if( usb.getDevice().parent().as<StorageDrive>()->driveType() == StorageDrive::MemoryStick ){
            label->setPixmap( KIcon("media-flash-memory-stick").pixmap(64, 64) );
        }
        else if( usb.getDevice().parent().as<StorageDrive>()->driveType() == StorageDrive::SmartMedia ){
            label->setPixmap( KIcon("media-flash-smart-media").pixmap(64, 64) );
        }
        else {
            //USB   
            label->setPixmap( KIcon("drive-removable-media-usb-pendrive").pixmap(64, 64) );
        }

  }
    
  enableGUI();
}

Device Window::getCurrentDevice()
{
  QString path = ui->cb_devices->currentText();
  USBDevice ud = devList.findDeviceFromPath(path);
  return ud.getDevice();
}

void Window::disableGUI()
{
  ui->widget->setDisabled(true);
}

void Window::enableGUI()
{
  ui->widget->setEnabled(true);
}

Ui::Window* Window::getUi()
{
  return ui;
}

#include "window.moc"
