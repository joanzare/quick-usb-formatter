/*
    <Interfaz Grafica>
    Copyright (C) <2011>  <José Antonio Sánchez Reynaga>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef WINDOW_H
#define WINDOW_H

#include <QtCore>
#include <QtGui>
#include "devices.h"
#include "usbdevice.h"
#include <KDE/KXmlGuiWindow>
#include <KDE/KStatusBar>
#include <KDE/KIcon>
#include <KDE/KAuth/Action>
#include <KDE/KAuth/ActionReply>
#include <KDE/KAuth/ActionWatcher>
#include <KDE/KAuth/HelperSupport>


namespace Ui {
    class Window;
}

using namespace KAuth;

class Window : public KXmlGuiWindow
{
    Q_OBJECT

public:
    explicit Window(DeviceList& dl, QWidget *parent = 0);
    ~Window();
    Ui::Window* getUi();

protected:
    virtual void closeEvent(QCloseEvent* e);
    
public slots:
    void refreshDeviceProperties();
    void newDeviceProperties(QString);
    void refreshListDevices(USBDevice, bool);    
    void enableGUI();
    void disableGUI();
    
    /**
     *  Se ejecuta cuando se autoriza una action
     */
    void performAction(KAuth::Action *action);
    
    

private:
    Ui::Window *ui;
    DeviceList &devList;
    Device getCurrentDevice();
    QString escapeLabel(const QString label);
    bool busy; 
    
};

#endif // WINDOW_H
